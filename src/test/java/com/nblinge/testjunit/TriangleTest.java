package com.nblinge.testjunit;


import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class TriangleTest {

    @Test
    @DisplayName(value="非三角形")
    public void NotTriangle()
    {
        Triangle triangle=new Triangle();
        String type=triangle.classify(10,20,30);
        assertEquals("非三角形",type);
    }
    @Test
    @DisplayName(value="等边三角形")
    public void TestTriangle()
    {
        Triangle triangle=new Triangle();
        String type=triangle.classify(10,10,10);
        assertEquals("等边三角形",type);
    }
    @Test
    @DisplayName(value="非等边三角形")
    public void TestCommon()
    {
        Triangle triangle=new Triangle();
        String type=triangle.classify(3,4,5);
        assertEquals("非等边三角形",type);
    }
    @Test
    @DisplayName(value="等腰三角形")
    public void TestHigh()
    {
        Triangle triangle=new Triangle();
        String type=triangle.classify(10,10,5);
        assertEquals("等腰三角形",type);
    }

    @ParameterizedTest
    @CsvSource({
            "10,20,30, 非三角形",
            "10,10,10, 等边三角形" ,
            "3,4,5, 非等边三角形",
            "10,10,5, 等腰三角形"

    })
    void paramTriangle(int x,int y,int z,String expected)
    {
            Triangle triangle=new Triangle();
        String  type=triangle.classify(x, y, z);
        assertEquals(expected,type);

    }


}

